<?php

use App\Controllers\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    // Método que muestra el inicio de sesión
    public function index()
    {
        $this->loadView('sign-in');
    }

    // Método que muestra el directorio de usuarios almacenados en la BD
    public function list()
    {
        if ($this->session->getCurrentUser()) {
            $users = App\Models\User::get();
            $this->loadView('directory', $users);
        } else {
            header('Location: ../');
        }
    }

    // Método que guarda los datos un usuario desde el formulario de registro
    public function create()
    {
        // Se limpian los datos que vienen del formulario
        $data = filter_var_array($_REQUEST, FILTER_SANITIZE_STRING);
        // Se encripta la clave
        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);

        try {
            $this->userModel->create($data);
        } catch (Exception $e) {
            echo 'Excepción: ',  $e->getMessage(), "\n";
        }

        header('Location: ../user');
    }

    // Método que muestra el formulario de registro de usuarios
    public function register()
    {
        if (!$this->session->getCurrentUser()) {
            $url = 'https://restcountries.eu/rest/v2';

            try {
                $countries = $this->customerModel->fetchData($url);
            } catch (Exception $e) {
                echo 'Excepción: ',  $e->getMessage(), "\n";
            }

            $this->loadView('register', $countries);
        } else {
            header('Location: ../user/list');
        }
    }

    // Importa los datos desde la API y los guarda en la BD
    public function import()
    {
        try {
            $this->customerModel->import();
        } catch (Exception $e) {
            echo 'Excepción: ',  $e->getMessage(), "\n";
        }

        header('Location: ../user/list');
    }

    // Valida los datos del formulario e inicia sesión
    public function login()
    {
        if (!$this->session->getCurrentUser()) {
            // Se limpian los datos que vienen del formulario
            $data = filter_var_array($_REQUEST, FILTER_SANITIZE_STRING);
            $user = $this->userModel->where('email', $data['email'])->get();
            $user = $user[0];
            // Se valida si la clave ingresada por el usuario es la de la BD
            $password = password_verify($data['password'], $user->password);

            if ($password && !empty($user)) {
                $this->session->setCurrentUser($user->email);
                $users = App\Models\User::get();

                $this->loadView('directory', $users);
            } else {
                header('Location: ../');
            }
        } else {
            header('Location: ../user/list');
        }
    }
}
