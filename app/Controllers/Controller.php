<?php

namespace App\Controllers;

use App\Controllers\SessionController;
use App\Models\User;
use App\Models\CustomerData;


class Controller
{
    protected $userModel;
    protected $customerModel;
    protected $session;

    public function __construct()
    {
        $this->userModel = new User;
        $this->customerModel = new CustomerData;
        $this->session = new SessionController;
    }

    public function loadView($view, $params = null)
    {
        require_once VIEWS_PATH . $view . '.php';
    }
}
