<?php

namespace App;

use App\Error;

class Router
{
    protected $name;
    protected $method;

    function __construct($name, $method)
    {
        $this->name = $name . 'Controller';

        $this->method = $method;

        $this->loadController();
    }

    public function loadController()
    {
        $file = __DIR__ . '/Controllers/' . $this->name . '.php';

        if (file_exists($file)) {

            include $file;
            $controller = new $this->name;

            if (method_exists($controller, $this->method)) {
                $controller->{$this->method}();
            } else {
                $error = new Error;
            }
        } else {
            $error = new Error;
        }
    }
}
