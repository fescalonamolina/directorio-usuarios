<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerData extends Model
{

    // Método que obtiene la data de una API, recibe la URL
    public static function fetchData($url)
    {
        $response = file_get_contents($url);
        $data = json_decode($response);

        return $data;
    }

    // Importa los usuarios desde la API y los guarda en la BD
    public function import()
    {
        $url = 'http://www.mocky.io/v2/5d9f39263000005d005246ae';
        $url2 = 'http://www.mocky.io/v2/5d9f38fd3000005b005246ac';
        $users = $this->fetchData($url)->objects;
        $users2 = $this->fetchData($url2)->objects;

        $this->storeUsers($users, true);
        $this->storeUsers($users2, false);
    }

    // Método que guardar los usuarios que se pasan como parámetro en la BD
    public function storeUsers($users, $english)
    {

        foreach ($users as $user) {

            if ($english) {
                $name = $user->first_name . ' ' . $user->last_name;
                $document = $user->document;
                $email = $user->email;
                $country = $user->country;
                $password = password_hash($document, PASSWORD_BCRYPT);
            } else {
                $name = $user->primer_nombre . ' ' . $user->apellido;
                $document = $user->cedula;
                $email = $user->correo;
                $country = $user->pais;
                $password = password_hash($document, PASSWORD_BCRYPT);
            }

            User::create([
                'name' => $name,
                'document' => $document,
                'email' => $email,
                'country' => $country,
                'password' => $password
            ]);
        }
    }
}
