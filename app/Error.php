<?php

namespace App;

class Error
{
    public function __construct()
    {
        echo '<strong>Error 404: No existe la ruta a la que intentas ingresar</strong>';
    }
}
