<?php require 'partials/header.php'; ?>

<div class="container mt-5">
    <div class="row d-flex flex-column align-items-center">
        <h3>Registro de usuarios</h3>
        <form class="mt-3" method="POST" action="create">
            <div class="form-group">
                <label for="name">Nombre</label>
                <input type="text" pattern=".{3,}" title="Mínimo 3 caracteres" class="form-control" id="name" placeholder="Name" name="name" required min="3">
            </div>
            <div class="form-group">
                <label for="document">Documento</label>
                <input type="text" class="form-control" id="document" placeholder="Documento" name="document" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
            </div>
            <div class="form-group">
                <label for="country">País</label>
                <select class="form-control" id="country" name="country" required>
                    <?php foreach ($params as $country) { ?>
                        <option><?= $country->name ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" pattern="(?=.*\d).{6,}" title="Debe contener al menos 1 número y mínimo 6 caracteres" class="form-control" id="password" name="password" required>
            </div>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Registrarse</button>
            </div>
        </form>
    </div>
</div>

<?php require 'partials/footer.php' ?>