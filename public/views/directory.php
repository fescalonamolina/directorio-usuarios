<?php

require 'partials/header.php';

?>

<div class="container">
    <div class="row d-flex flex-column">
        <h3 class="mt-4 mb-3 text-center">Directorio de Usuarios</h3>
        <a href="import" class="text-center mt-3">
            <button class="btn btn-primary"> Importar Usuarios desde API </button>
        </a>
        <table id="usersTable" class="table table-striped table-bordered table-sm" cellspacing="0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Documento</th>
                    <th scope="col">Email</th>
                    <th scope="col">País</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($params as $user) {
                ?>
                    <tr>
                        <th scope="row"><?= $i ?></th>
                        <td><?= $user->name ?></td>
                        <td><?= $user->document ?></td>
                        <td><?= $user->email ?></td>
                        <td><?= $user->country ?></td>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>

        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#usersTable').DataTable({
            scrollY: 400,
            paging: true,
            searching: true,
            ordering: true
        });
    });
</script>

<?php require 'partials/footer.php'; ?>