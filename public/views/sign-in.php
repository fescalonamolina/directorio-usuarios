<?php require 'partials/header.php'; ?>

<div class="container mt-5 pt-5">
    <div class="row d-flex flex-column align-items-center">
        <h3 class="mb-4">Iniciar Sesión</h3>
        <form method="POST" action="user/login">
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input type="password" pattern="(?=.*\d).{6,}" title="Debe contener al menos 1 número y mínimo 6 caracteres" class="form-control" id="password" name="password" required>
            </div>
            <p>¿Aún no tienes cuenta? <a href="user/register">Registrarse</a></p>
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Iniciar Sesión</button>
            </div>
        </form>
    </div>
</div>

<?php require 'partials/footer.php' ?>