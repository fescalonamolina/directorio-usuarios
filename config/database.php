<?php

use Illuminate\Database\Capsule\Manager as Connection;

$connection = new Connection;

$connection->addConnection([
    'driver'   => 'mysql',
    'host'     => 'localhost',
    'database' => 'prueba_fescalona',
    'username' => 'root',
    'password' => '',
    'charset'  => 'utf8',
    'prefix'   => '',
]);

$connection->bootEloquent();
