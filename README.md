## Directorio de usuarios

Directorio de Usuarios desarrollado con PHP.

### Características

- ORM: Eloquent.
- URL amigables.
- Front end con Bootstrap.
- Motor de Base de Datos: MySQL.


### Pasos para la instalación del proyecto

```sh

1. Clonar el repositorio
git clone https://gitlab.com/fescalonamolina/directorio-usuarios.git

2. Ir al directorio del proyecto
cd directorio-usuarios

3. Instalar las dependencias
composer install

4. Entrar a MySQL y ejecutar los queries del archivo deploy.sql que se encuentra en la raíz del proyecto.


```

### Abrir en el navegador la URL

```sh
localhost/directorio-usuarios
```

## License

Directorio de usuarios [licensed as MIT](https://github.com/facebook/create-react-app/blob/master/LICENSE).