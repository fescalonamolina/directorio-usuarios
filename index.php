<?php

require_once 'vendor/autoload.php';
require_once 'config/database.php';
require_once 'config/config.php';

use App\Router;
use App\Controllers\SessionController;

$url = $_GET['url'];
$url = rtrim($url, '/');
$url = explode('/', $url);

if ($url[0] == 'session' && $url[1] == 'close') {
    $session = new SessionController;
    $session->close();
} elseif (empty($url[0])) {
    $controllerName = 'User';
} else {
    $controllerName = ucfirst($url[0]);
}

if (isset($url[1])) {
    $method = $url[1];
} else {
    $method = 'index';
}

$router = new Router($controllerName, $method);
