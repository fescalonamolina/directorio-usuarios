-- Crear la base de datos

CREATE DATABASE prueba_fescalona;

-- Usar la Base de datos creada:

USE DATABASE prueba_fescalona;

-- Crear la tabla users:

CREATE TABLE users
(id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR (255) NOT NULL,
document VARCHAR (255) NOT NULL UNIQUE,
email VARCHAR (255) NOT NULL UNIQUE,
country VARCHAR (255) NOT NULL,
password VARCHAR (255) NOT NULL,
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP);

-- Insertar usuarios de prueba:

INSERT INTO users
    (name, document, email, country, password)
VALUES
    ('Gerardo Escalona', '684455', 'fescalonamolina@gmail.com', 'Colombia', '$2y$10$/AX6IrwdN3xG9ETD/30pOOdxZEwyz1lZcSSygDMqra63YaYmCBbg6');

INSERT INTO users
    (name, document, email, country, password)
VALUES
    ('John Doe', '12345678', 'demo@app.com', 'Estados Unidos', '$2y$10$/AX6IrwdN3xG9ETD/30pOOdxZEwyz1lZcSSygDMqra63YaYmCBbg6');
